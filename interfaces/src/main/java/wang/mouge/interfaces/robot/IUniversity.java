package wang.mouge.interfaces.robot;

/**
 * Robots接口实现类
 * 
 * @author kangbin <kangbin@mouge.wang>
 * @author chenmin <chenmin@mouge.wang>
 * @since 2015-04-02
 */
public interface IUniversity
{
	/**
	 * 获取验证码
	 * 
	 * @param schoolId
	 *            学校ID
	 * @param studentId
	 *            学生ID
	 * @return 验证码地址
	 */
	public abstract String getCaptcha(String schoolId, String studentId);

	/**
	 * 登录系统
	 * 
	 * @param schoolId
	 *            学校ID
	 * @param studentId
	 *            学生ID
	 * @param studentPwd
	 *            学生密码
	 * @param captcha
	 *            验证码
	 * @return 登录结果
	 */
	public abstract String login(String schoolId, String studentId, String studentPwd, String captcha);

	/**
	 * 更新学生的信息
	 * 
	 * @param token
	 *            学生token
	 * @return
	 */
	public abstract void update(String token);

}
