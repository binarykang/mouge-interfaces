package wang.mouge.interfaces.dubbo;

/**
 * 配置文件构造器
 * 
 * @author dongchao <dongchao@mouge.wang>
 * @since 2015-07-23
 */
public class DubboConfigUrl
{
	public static String controllerUrl()
	{
		String configUrl = "http://xml.dubbo.mengxiaozhu.cn/config/xmu_mxz_restful_controller/group/";

		if (Debug.url_value)
			configUrl = configUrl + Debug.online_debug;
		else
			configUrl = configUrl + Debug.online_release;

		return configUrl;
	}

	public static String robotsUrl()
	{
		String configUrl = "http://xml.dubbo.mengxiaozhu.cn/config/xmu_mxz_robots/group/";

		if (Debug.url_value)
			configUrl = configUrl + Debug.online_debug;
		else
			configUrl = configUrl + Debug.online_release;

		return configUrl;
	}
}
