package wang.mouge.interfaces.dubbo;

/**
 * 部署服务器调试开关
 * 
 * @author dongchao <dongchao@mouge.wang>
 * @since 2015-07-23
 */
public class Debug
{
	/*
	 * 1.发布新版的时候请将两个开关都关闭
	 * 2.本地进行url测试的时候请开启url_value,并关闭console_value
	 * 3.本地控制台进行调试的时候请开启console_value（url_value开启无所谓）
	 */

	// 本地url测试开关
	public static final boolean url_value = false;

	public static final String online_release = "xmu_release";

	public static final String online_debug = "xmu_debug_dc";
}
